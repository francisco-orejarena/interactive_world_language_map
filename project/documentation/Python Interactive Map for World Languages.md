# Python Interactive Map for World Languages

### My Take on a Map of WALS Data

The purpose of this document is detail my work and progress on the interactive map of the world's languages for CSIT 505: Python Programming.

Ideally, the document will be laid out in the following manner:

- a date as the subheader, or some similar categorical separation
- the goal of that day's work
- the completion of that day's work
- what I learned from that day's work

I would like to keep this document throughout the 5 weeks so I can delineate the progress made, as well as keep healthy programming practices and learn for my professional future.

## Wednesday May 26th, 2021

Today, I worked on setting myself to do daily work towards this project. My overall goals for the project are as follow:

1. Learn Markdown (or any markup language, right now it's Markdown lol)
2. Learn how to effectively use git, so I can work on version control of my project and submit to GitLab
3. Work on python

A few things to go over in this... journal, of sorts, is that I will not be using GitHub. The reason being that I do not find GitHub's practices, as a company, ethical. You can see that in the following:

1. Firing Jewish Employee for Concern post-January 6th:
    - [Fired GitHub employee reaches ‘amicable resolution’ with company](https://techcrunch.com/2021/03/15/fired-github-employee-reaches-amicable-resolution-with-company/?guccounter=1&guce_referrer=aHR0cHM6Ly93d3cuZ29vZ2xlLmNvbS8&guce_referrer_sig=AQAAAMcyQCaNKw8XaQpCTe1mkHojlUQ1tp9R-7GNRwYnS6JxMfVpj9mm-d4x53nm3JuXxRqCt9_GZeIR6YGWfvv8GyU0fwka5KnnBTCjqtGYemEAtbhsEwNTobIqu22rZ_4KcbFJ6Ai7aQdmDEH-Dv-5MwfjOod1dA-YtPZpgRjm84mM)
    - [GitHub apologizes for firing Jewish employee who warned of Nazis in U.S. Capitol - Reuters](https://www.bbc.com/news/technology-55704932)
2. Removing FOSS from Website:
    - [GitHub reinstated a YouTube video downloader that the RIAA claimed was a piracy tool](https://www.theverge.com/2020/11/17/21571473/github-youtube-dl-downloader-riaa-copyright-1201-takedown-reinstated)

It is for these reasons that I will not be using GitHub. Instead, I will be using GitLab, which is open-source and seemingly a more ethical company.

Another aspect of my learning is that I want to learn to use tools for ease of programming and healthy practice; for example, I've been learning LaTeX at the beginning of the Spring 2021 semester, and submitted *almost* all of my homework as a PDF made using LaTeX. With this in mind, I want to think of more practical things to learn for programming specifically. These are going to be **Markdown**, **git**, and obviously **python**.