import folium

# Creating a map
m = folium.Map(location=[42.3601, -71.0589], zoom_start=12)

# Create markers
folium.Marker([42.363600, -71.099500],
				popup='<strong>Location One</strong>',
				tooltip=tooltip).add_to(m)

# Generate map
m.save('map.html')