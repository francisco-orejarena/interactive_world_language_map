"""
The following is the code for the project for Python Programming (CSIT 505).
I will start this file and hopefully be able to work on it daily and get 
through the markers that I have designated for myself, as well as learn to
define better goals and milestones along the way.
"""

"""
Necessary things to copy and paste:
C:/Users/Francisco/Desktop/Montclair/Summer 2021/Python Programming/interactive_world_language_map/project/datasets/
"""




# Packages for the project
import csv
import datapackage
import geojson as geoj
import json
import mapbox as maps
import math
import pandas as pd
import plotly
import plotly.express as px
import time


# Testing the GeoJSON file for countries around the world
data_url = "https://datahub.io/core/geo-countries/datapackage.json"

package = datapackage.Package(data_url)

resources = package.resources
for resource in resources:
	if resource.tabular:
		data = pd.read_csv(resource.descriptor['path'])
		print(data) # No issues in the code


# Printing part of the GeoJSON
world_countries = json.load(open("C:/Users/Francisco/Desktop/Montclair/Summer 2021/Python Programming/interactive_world_language_map/project/datasets/countries.geojson", "r"))


#print(len(world_countries['features'])) # Output: 255

"""
n = 0
while n < len(world_countries['features']):
	print(world_countries['features'][n]['properties'])
	n += 1 # This works
"""






"""
In the following section, I am going to be working on the CSV data.
"""

df = pd.read_csv("C:/Users/Francisco/Desktop/Montclair/Summer 2021/Python Programming/interactive_world_language_map/project/datasets/language.csv")
print(df.head()) # This works

#print(df['countrycodes']) This works




fig = px.choropleth(
    df,
    locations="countrycodes",
    geojson=world_countries,
    hover_name="countrycodes",
    hover_data=["Name"],
    title="World Languages",
)
fig.update_geos(fitbounds="locations", visible=False)
fig.show()