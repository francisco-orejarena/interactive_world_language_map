# -*- coding: utf-8 -*-
"""
Created on Fri May 28 22:56:31 2021

@author: Francisco
"""


import pandas as pd
#import numpy as np

df1 = pd.read_csv('language.csv')
df1.head()

f = open("language.csv", "r")
print(f.read(1000)) 


# importing the csv library
import csv
  
# opening the csv file by specifying
# the location
# with the variable name as csv_file
with open('wals-data.csv') as csv_file:
  
    # creating an object of csv reader
    # with the delimiter as ,
    csv_reader = csv.reader(csv_file, delimiter = ',')
  
    # list to store the names of columns
    list_of_column_names = []
  
    # loop to iterate thorugh the rows of csv
    for row in csv_reader:
  
        # adding the first row
        list_of_column_names.append(row)
  
        # breaking the loop after the
        # first iteration itself
        break
  
# printing the result
print("List of column names : ",
      list_of_column_names[0])