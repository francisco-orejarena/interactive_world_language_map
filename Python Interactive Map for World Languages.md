# Python Interactive Map for World Languages

### My Take on a Map of WALS Data

The purpose of this document is detail my work and progress on the interactive map of the world's languages for CSIT 505: Python Programming.

Ideally, the document will be laid out in the following manner:

- a date as the subheader, or some similar categorical separation
- the goal of that day's work
- the completion of that day's work
- what I learned from that day's work

I would like to keep this document throughout the 5 weeks so I can delineate the progress made, as well as keep healthy programming practices and learn for my professional future.

## Wednesday May 26th, 2021

Today, I worked on setting myself to do daily work towards this project. My overall goals for the project are as follow:

1. Learn Markdown (or any markup language, right now it's Markdown lol)
2. Learn how to effectively use git, so I can work on version control of my project and submit to GitLab
3. Work on python

A few things to go over in this... journal, of sorts, is that I will not be using GitHub. The reason being that I do not find GitHub's practices, as a company, ethical. You can see that in the following:

1. Firing Jewish Employee for Concern post-January 6th:
    - [Fired GitHub employee reaches ‘amicable resolution’ with company](https://techcrunch.com/2021/03/15/fired-github-employee-reaches-amicable-resolution-with-company/?guccounter=1&guce_referrer=aHR0cHM6Ly93d3cuZ29vZ2xlLmNvbS8&guce_referrer_sig=AQAAAMcyQCaNKw8XaQpCTe1mkHojlUQ1tp9R-7GNRwYnS6JxMfVpj9mm-d4x53nm3JuXxRqCt9_GZeIR6YGWfvv8GyU0fwka5KnnBTCjqtGYemEAtbhsEwNTobIqu22rZ_4KcbFJ6Ai7aQdmDEH-Dv-5MwfjOod1dA-YtPZpgRjm84mM)
    - [GitHub apologizes for firing Jewish employee who warned of Nazis in U.S. Capitol - Reuters](https://www.bbc.com/news/technology-55704932)
2. Removing FOSS from Website:
    - [GitHub reinstated a YouTube video downloader that the RIAA claimed was a piracy tool](https://www.theverge.com/2020/11/17/21571473/github-youtube-dl-downloader-riaa-copyright-1201-takedown-reinstated)

It is for these reasons that I will not be using GitHub. Instead, I will be using GitLab, which is open-source and seemingly a more ethical company.

Another aspect of my learning is that I want to learn to use tools for ease of programming and healthy practice; for example, I've been learning LaTeX at the beginning of the Spring 2021 semester, and submitted *almost* all of my homework as a PDF made using LaTeX. With this in mind, I want to think of more practical things to learn for programming specifically. These are going to be **Markdown**, **git**, and obviously **python**.

## Friday June 4th, 2021

Over the last week I worked on:

- establishing what package I was going to use for the map outline
- gathering tutorials for certain aspect of the project that werer similar
- reading data in excel and seeing what columns I could use
- essentially forgetting to try and get at least an hour work in daily for healthy programming practice.

I did take some code from the plotly website to try and get a world map. It worked-  literally just running it and being able to view a static world map. None of the countries are clickable, there is no linked information, I don't even know if that's the map that I want to stick with.

## Saturday June 5th, 2021

I watched a [video](https://www.youtube.com/watch?v=4RnU5qKTfYY&list=PLJuSgNUwatei3BWUKsyPYQXKxlYmXPLyj&index=2) today about  using folium to get access to a world map. The map that I made after following the tutorial is like a smaller version of google maps, it is great- still unsure of how I would make it interactive, though. This is a large task, I still need to see how I cam going to do a rough version of this project. Unsure of the map that I will use.

Another [video](https://www.youtube.com/watch?v=aJmaw3QKMvk&list=PLJuSgNUwatei3BWUKsyPYQXKxlYmXPLyj&index=4) that I watched was a tutorial using plotly chloropleth maps. This data visualization seems like the one that I should use for my project because the clear country wide boundaries are easy to show (compared to that of `folium`), and this tutorial discusses using a csv file format.

One thing that I seemed to have forgotten to explain is that I took my data from World Atlas of Language Structures (WALS), which is in a CSV format, and I am pretty sure that it is cleaned data. One issue seems to be that one of the csv files has a massive amound of data and categories, and the other one feels completely different in structure and layout, as it is smaller. What I need to do is to try and figure out what their individual purposes are in terms of data collection.



